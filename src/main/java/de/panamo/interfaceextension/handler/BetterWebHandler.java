package de.panamo.interfaceextension.handler;


import de.dytanic.cloudnet.lib.NetworkUtils;
import de.dytanic.cloudnet.lib.map.WrappedMap;
import de.dytanic.cloudnet.lib.player.CloudPlayer;
import de.dytanic.cloudnet.lib.server.*;
import de.dytanic.cloudnet.lib.server.advanced.AdvancedServerConfig;
import de.dytanic.cloudnet.lib.server.template.Template;
import de.dytanic.cloudnet.lib.server.template.TemplateResource;
import de.dytanic.cloudnet.lib.server.version.ProxyVersion;
import de.dytanic.cloudnet.lib.user.User;
import de.dytanic.cloudnet.lib.utility.document.Document;
import de.dytanic.cloudnet.web.server.handler.WebHandler;
import de.dytanic.cloudnet.web.server.util.PathProvider;
import de.dytanic.cloudnet.web.server.util.QueryDecoder;
import de.dytanic.cloudnetcore.CloudNet;
import de.dytanic.cloudnetcore.database.StatisticManager;
import de.dytanic.cloudnetcore.network.components.MinecraftServer;
import de.dytanic.cloudnetcore.network.components.ProxyServer;
import de.dytanic.cloudnetcore.network.components.Wrapper;
import de.dytanic.cloudnetcore.network.components.WrapperMeta;
import de.dytanic.cloudnetcore.util.defaults.BasicProxyConfig;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.FullHttpResponse;
import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.codec.http.HttpResponseStatus;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

public class BetterWebHandler extends WebHandler {
    private Collection<String> noPermissionRequests = new ArrayList<>();

    public BetterWebHandler() {
        super("/cloudnet/api/v2/util");
        noPermissionRequests.add("userinfo");
    }

    @Override
    public FullHttpResponse handleRequest(ChannelHandlerContext channelHandlerContext, QueryDecoder queryDecoder, PathProvider pathProvider, HttpRequest httpRequest) throws Exception {
        FullHttpResponse fullHttpResponse = new DefaultFullHttpResponse(httpRequest.getProtocolVersion(), HttpResponseStatus.UNAUTHORIZED);
        fullHttpResponse.headers().set("Content-Type", "application/json");

        Document dataDocument = new Document("success", false).append("reason", new ArrayList<>()).append("response", new Document());
        if (!httpRequest.headers().contains("-Xcloudnet-user") || (!httpRequest.headers().contains("-Xcloudnet-token") && !httpRequest.headers().contains("-Xcloudnet-password")) || !httpRequest.headers().contains("-Xmessage")) {
            dataDocument.append("reason", Arrays.asList("-Xcloudnet-user, -Xcloudnet-token or -Xmessage not found!"));
            fullHttpResponse.content().writeBytes(dataDocument.convertToJsonString().getBytes(StandardCharsets.UTF_8));
            return fullHttpResponse;
        }

        if (httpRequest.headers().contains("-Xcloudnet-token") ? !CloudNet.getInstance().authorization(httpRequest.headers().get("-Xcloudnet-user"), httpRequest.headers().get("-Xcloudnet-token")) : !CloudNet.getInstance().authorizationPassword(httpRequest.headers().get("-Xcloudnet-user"), httpRequest.headers().get("-Xcloudnet-password"))) {
            dataDocument.append("reason", Arrays.asList("failed authorization!"));
            fullHttpResponse.content().writeBytes(dataDocument.convertToJsonString().getBytes(StandardCharsets.UTF_8));
            return fullHttpResponse;
        }

        User user = CloudNet.getInstance().getUser(httpRequest.headers().get("-Xcloudnet-user"));
        String message = httpRequest.headers().get("-Xmessage").toLowerCase();
        String value = httpRequest.headers().contains("-Xvalue") ? httpRequest.headers().get("-Xvalue") : "";

        if (!this.noPermissionRequests.contains(message) && !user.getPermissions().contains("cloudnet.web." + message) && !user.getPermissions().contains("*")) {
            dataDocument.append("reason", Arrays.asList("permission denied!", "cloudnet.web." + message));
            fullHttpResponse.content().writeBytes(dataDocument.convertToJsonString().getBytes(StandardCharsets.UTF_8));
            fullHttpResponse.setStatus(HttpResponseStatus.FORBIDDEN);
            return fullHttpResponse;
        }

        dataDocument.append("success", true);
        switch (message) {
            case "serverinfos":
                Document response = new Document();
                for (MinecraftServer minecraftServer : CloudNet.getInstance().getServers().values())
                    response.append(minecraftServer.getServiceId().getServerId(), minecraftServer.getServerInfo());

                dataDocument.append("response", response);
                break;
            case "proxyinfos":
                Document proxyResponse = new Document();
                for (ProxyServer minecraftServer : CloudNet.getInstance().getProxys().values())
                    proxyResponse.append(minecraftServer.getServiceId().getServerId(), minecraftServer.getProxyInfo());

                dataDocument.append("response", proxyResponse);
                break;
            case "userinfo":
                Document userResponse = new Document();
                userResponse.append("name", user.getName());
                userResponse.append("uuid", user.getUniqueId().toString());
                userResponse.append("permissions", user.getPermissions());

                dataDocument.append("response", userResponse);
                break;
            case "onlineplayers":
                Document playerResponse = new Document();
                for (CloudPlayer cloudPlayer : CloudNet.getInstance().getNetworkManager().getOnlinePlayers().values())
                    playerResponse.append(cloudPlayer.getUniqueId().toString(), cloudPlayer);

                dataDocument.append("response", playerResponse);
                break;
            case "statistics":
                dataDocument.append("response", StatisticManager.getInstance().getStatistics());
                break;
            case "cloudnetwork":
                dataDocument.append("response", CloudNet.getInstance().getNetworkManager().newCloudNetwork());
                break;
            case "memory":
                Document memoryDocument = new Document()
                        .append("maxMemory", CloudNet.getInstance().globalMaxMemory())
                        .append("usedMemory", CloudNet.getInstance().globalUsedMemory());
                dataDocument.append("response", memoryDocument);
                break;
            case "cpuusage":
                double cpuUsage = 0.0D;
                for(Wrapper wrapper : CloudNet.getInstance().getWrappers().values())
                    cpuUsage += wrapper.getCpuUsage();
                cpuUsage /= CloudNet.getInstance().getWrappers().size();
                dataDocument.append("response", cpuUsage);
                break;
            case "startserver":
                CloudNet.getInstance().getScheduler().runTaskSync(() -> CloudNet.getInstance().startGameServer(CloudNet.getInstance().getServerGroup(value)));
                break;
            case "startproxy":
                CloudNet.getInstance().getScheduler().runTaskSync(() -> CloudNet.getInstance().startProxy(CloudNet.getInstance().getProxyGroup(value)));
                break;
            case "stopserver":
                CloudNet.getInstance().getScheduler().runTaskSync(() -> CloudNet.getInstance().stopServer(value));
                break;
            case "stopproxy":
                CloudNet.getInstance().getScheduler().runTaskSync(() -> CloudNet.getInstance().stopProxy(value));
                break;
            case "log":
                MinecraftServer minecraftServer = CloudNet.getInstance().getServer(message);
                if (minecraftServer != null) {
                    String randomString = NetworkUtils.randomString(10);
                    CloudNet.getInstance().getServerLogManager().append(randomString, minecraftServer.getServerId());
                    String logLink = new StringBuilder(CloudNet.getInstance().getOptionSet().has("ssl") ? "https://" : "http://").append(CloudNet.getInstance().getConfig().getWebServerConfig()
                            .getAddress()).append(":").append(CloudNet.getInstance().getConfig().getWebServerConfig().getPort()).append("/cloudnet/log?server=").append(randomString).substring(0);
                    dataDocument.append("response", logLink);
                } else
                    dataDocument.append("response", "No server found");

                break;
            case "createwrapper":
                String[] values = value.split(",");
                WrapperMeta wrapperMeta = new WrapperMeta(values[0], values[1], values[2]);
                CloudNet.getInstance().getConfig().createWrapper(wrapperMeta);
                break;
            case "createservergroup":
                String[] serverGroupValues = value.split(",");

                ServerGroup serverGroup = new ServerGroup(
                        serverGroupValues[0],
                        Collections.singletonList(serverGroupValues[1]),
                        true,
                        Integer.valueOf(serverGroupValues[2]),
                        Integer.valueOf(serverGroupValues[2]),
                        0,
                        false,
                        Integer.valueOf(serverGroupValues[3]),
                        Integer.valueOf(serverGroupValues[4]),
                        Integer.valueOf(serverGroupValues[5]),
                        180,
                        100,
                        100,
                        Integer.valueOf(serverGroupValues[6]),
                        ServerGroupType.valueOf(serverGroupValues[7]),
                        ServerGroupMode.valueOf(serverGroupValues[8]),
                        Arrays.asList(new Template(
                                "default",
                                TemplateResource.valueOf(serverGroupValues[9]),
                                null,
                                new String[0],
                                new ArrayList<>()
                        )),
                        new AdvancedServerConfig(false, false, false, !ServerGroupMode.valueOf(serverGroupValues[8]).equals(ServerGroupMode.STATIC)));
                CloudNet.getInstance().getConfig().createGroup(serverGroup);
                break;
            case "createproxygroup":
                String[] proxyGroupValues = value.split(",");
                ProxyGroup proxyGroup = new ProxyGroup(proxyGroupValues[0], Collections.singletonList(proxyGroupValues[1]), new Template(
                        "default",
                        TemplateResource.valueOf(proxyGroupValues[2]),
                        null,
                        new String[0],
                        new ArrayList<>()
                ), ProxyVersion.BUNGEECORD, Integer.valueOf(proxyGroupValues[3]), Integer.valueOf(proxyGroupValues[4]), Integer.valueOf(proxyGroupValues[5]), new BasicProxyConfig(),
                        ProxyGroupMode.valueOf(proxyGroupValues[6]), new WrappedMap());
                CloudNet.getInstance().getConfig().createGroup(proxyGroup);
                break;
            case "deleteservergroup":
                CloudNet.getInstance().getConfig().deleteGroup(CloudNet.getInstance().getServerGroup(value));
                break;
            case "deleteproxygroup":
                CloudNet.getInstance().getConfig().deleteGroup(CloudNet.getInstance().getProxyGroup(value));
                break;
            default:
                dataDocument.append("reason", Arrays.asList("No available -Xmessage command found!"));
                break;
        }

        fullHttpResponse.setStatus(HttpResponseStatus.OK);
        fullHttpResponse.content().writeBytes(dataDocument.convertToJsonString().getBytes(StandardCharsets.UTF_8));

        return fullHttpResponse;
    }

}
