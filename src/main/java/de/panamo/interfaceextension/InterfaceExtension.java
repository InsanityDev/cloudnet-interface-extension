package de.panamo.interfaceextension;

import de.dytanic.cloudnetcore.api.CoreModule;
import de.panamo.interfaceextension.handler.AuthHandler;
import de.panamo.interfaceextension.handler.BetterWebHandler;

public class InterfaceExtension extends CoreModule {

    @Override
    public void onBootstrap() {
        super.getCloud().getWebServer().getWebServerProvider().registerHandler(new BetterWebHandler());
        super.getCloud().getWebServer().getWebServerProvider().registerHandler(new AuthHandler(this));
    }
}
