## Info

This is the module for the CloudNet-Interface, which you have to put in your
modules-folder in the CloudNet-Master.

## Using

The interface and module are far from beeing finished and there are not many features
implemented yet, but if you still want to use them and test the current features,
you can download the code and run this command in the directory of where you
saved it: 

```bash
$ mvn package
```

Notice: You need [Maven](https://maven.apache.org/guides/getting-started/maven-in-five-minutes.html) for this.
